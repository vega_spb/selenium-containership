import os
from multiprocessing import cpu_count
from multiprocessing import Process
import redis
from rq import Worker, Queue, Connection

"""
listen = ['default', 'slow']

redis_url = os.getenv('REDISTOGO_URL', 'redis://localhost:6379')

conn = redis.from_url(redis_url)

if __name__ == '__main__':
    with Connection(conn):
        worker = Worker(list(map(Queue, listen)))
        worker.work()
"""


class MyWorker:
    def __init__(self, workers_amount=int(cpu_count()/2)):
        self.workers_amount = workers_amount
        self.listen = ['default', 'slow']
        self._redis_url = os.getenv('REDISTOGO_URL', 'redis://localhost:6379')
        self.conn = redis.from_url(self._redis_url)
        self.workers = []
        self.processes = []

    def _start_worker(self, queues=['default']):
        try:
            with Connection(self.conn):
                worker = Worker(list(map(Queue, queues)))
                self.workers.append(worker)
                process = Process(target=worker.work)
                process.start()
        except ConnectionError as e:
            print(e)
#            sys.exit(1)
        return process

    def dadada(self):
        print('Initializing %s workers to serve queues' % self.workers_amount)
        for i in range(self.workers_amount):
            queues = []
            if i % 2 == 0:
                queues = self.listen
            else:
                queues.append(self.listen[0])
            self.processes.append(self._start_worker(queues))
            print('initialized worker to serve %s queues' % self.workers[i].queue_names())
        for worker in self.workers:
            print('worker for serving queue %s is established' % worker.queue_names())


workers = MyWorker()

