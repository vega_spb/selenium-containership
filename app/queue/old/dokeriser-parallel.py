import docker as DOCKER
from multiprocessing import Process
from queue import Queue
from threading import Thread
import re

class Worker(Thread):
    """Thread executing tasks from a given tasks queue"""
    def __init__(self, tasks):
        Thread.__init__(self)
        self.tasks = tasks
        self.daemon = True
        self.start()

    def run(self):
        while True:
            func, args, kargs = self.tasks.get()
            try:
                func(*args, **kargs)
            except Exception as e:
                print(e)
            finally:
                self.tasks.task_done()

class ThreadPool:
    """Pool of threads consuming tasks from a queue"""
    def __init__(self, num_threads):
        self.tasks = Queue(num_threads)
        for _ in range(num_threads):
            Worker(self.tasks)

    def add_task(self, func, *args, **kargs):
        """Add a task to the queue"""
        self.tasks.put((func, args, kargs))

    def wait_completion(self):
        """Wait for completion of all the tasks in the queue"""
        self.tasks.join()

class Ruler:
    def __init__(self):
        Process.__init__(self)
        self.doc = DOCKER.from_env()
        self.pool = ThreadPool(5)
        self.run_hub()

    def run_hub(self):
        selenium_net_present = False
        hub_counter = 0
#       check and init if necessary network
        for net in self.doc.networks.list():
            if self.doc.networks.get(net.id).attrs['Name'] == 'grid':
                selenium_net_present = True
        if selenium_net_present == False:
            self.doc.networks.create('grid')
#       counting hubs and init if necessary
        for container in self.doc.containers.list():
            if re.search('(selenium\/hub)', str(container.image)):
                hub_counter += 1
        if hub_counter >= 2:
            for container in self.doc.containers.list():
                if re.search('(selenium\/hub)', str(container.image)):
                    container.stop()
            self.doc.containers.run('selenium/hub:latest', ports={4444: 4444}, network='grid')
        elif hub_counter == 1:
            pass
        else:
            self.doc.containers.run('selenium/hub:latest', ports={4444: 4444}, network='grid')

    def import_image(self, image_name):
        self.pool.add_task(self.doc.images.pull, image_name)

    def start_container(self, image_name):
        #ports = { 4444: "" }
        ports = None
        print('starting container', image_name)
        self.pool.add_task(self.doc.containers.run, image_name,  network='grid')

    def stop_container(self, container_id):
        container = self.doc.containers.get(container_id)
        self.pool.add_task(container.stop)