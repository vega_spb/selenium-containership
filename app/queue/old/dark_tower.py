from app.routes.tower import doc

import asyncio

import concurrent.futures


from multiprocessing import Process, current_process
from multiprocessing.dummy import Pool as ThreadPool

import logging

class Shultz():

    """
    task is dictionary
    task {'id': id, '<job_type>': payload, 'in_work': T/F}
    job_type = add_image | start_container | etc
    """

    def add_task(self, task):
        print('Task is:', task, 'pid is', current_process().pid)
        self.counter += 1
        task_dict = {self.counter: task}
        self.queue.update(task_dict)

    def get_task(self):
        for key, task in self.queue:
            if not(task['in_work']):
                return task
            else:
                return None
        return None

    def in_work(self, id):
        self.queue[id]['in_work'] = True
        return 'OK'

    def task_done(self, id):
        self.queue.pop(id)
        return 'You are free now'

    async def handler(self):
        logging.info('printing from run(), pid is: %s' % current_process().pid)
        print('printing from run(), pid is: ', current_process().pid)
        while self.evaluation > 5:
            if self.queue.keys():
                for id, task in self.queue:
                    self.in_work(id)
                    self.djangoz.map(self.django,task)

    def django(self,task):
        print('Django got a task!', task)
        if task is dict:
            if task['in_work']:
                pass
            else:
                if task['add_image']:
                    print('Adding image %s' % task['add_image'])
                    doc.images.pull(task['add_image'])
                    self.task_done(task['id'])
                elif task['start_container']:
                    print(task['start_container'])
                    cont = doc.containers.run(task['start_container'], network=network, environment=cont_env, ports=ports, detach=True)
                    print(cont)
                elif task['stop_container']:
                    cont = doc.containers.get(task['stop_container'])
                    cont.stop()
        print('Django is free now')

    def start(self, and_loop=True):
        self._handler = self._loop.run_until_complete(self._handler)
        logging.info('Starting job handler')
        if and_loop:
            self._loop.run_forever()

    def stop(self, and_loop=True):
        self.evaluation = 0
        self._handler.close()
        if and_loop:
            self._loop.close()

    def pre_run(self):
        self._loop.run_until_complete(self._handler)

    def __init__(self):
#        logging.basicConfig(level=logging.DEBUG)
        self.queue = {}
        self.counter = 0
        self.djangoz = ThreadPool(4)
        self.evaluation = 100
        print('printing from init, pid is ', current_process().pid)
#        logging.info('printing from __init__, pid is: %s' % current_process().pid)
        self._loop = asyncio.get_event_loop()
        self._handler = self._loop.create_task(self.handler())
        self._loop.run_in_executor(None, self.handler())

