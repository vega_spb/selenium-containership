import re
import logging
import docker as DOCKER
import time
# redis, redis queue
from rq import Queue
from app.queue.worker import workers

#from app.logger import log


doc = DOCKER.from_env()
log = logging.getLogger('docker')


def start_new_hub(image, kwargs):
    pass


def network_check( network):
    print('Checking net')
    if doc.networks.list(names=[network]):
        print('Net is OK')
    else:
        doc.networks.create(network)
        print('Net is not ok, creating new one')


def start_new_container(image, **kwargs):
    params = dict()
    params['ports'] = {5900: None}
    params['network'] = 'grid'
    params['environment'] = {'HUB_HOST': 'selenium-hub', 'HUB_PORT': 4444}
    params['detach'] = True
    params['links'] = {'selenium-hub': 'hub'}
    for key in params:
        if key in kwargs:
            params[key] = kwargs[key]

    network_check(params['network'])

    if 'HUB_HOST' and 'HUB_PORTS' in kwargs.items():
        params['environment'] = {'HUB_HOST': kwargs['HUB_HOST'], 'HUB_PORT': kwargs['HUB_PORT']}

    if re.search('(^selenium/hub)', image):
        params['environment'] = None
        params.pop('links')
        if 'name' in kwargs.items():
            try:
                hub = doc.containers.get(kwargs['name'])
                hub.start()
            except DOCKER.errors.NotFound:
                params['name'] = kwargs['name']
                doc.containers.run(image, **params)
        else:
            params['name'] = 'selenium-hub'
            try:
                hub = doc.containers.get('selenium-hub')
                if hub.status == 'exited':
                    hub.start()
                    return 'started current default hub'
                else:
                    params.pop('name')
                    doc.containers.run(image, **params)
                    return 'started'
            except DOCKER.errors.NotFound:
                params['ports'] = {4444: 4444}
                doc.containers.run(image, **params)
                return 'created new one default hub'

        if 'ports' in kwargs.items():
            hubs = doc.containers.list(filters={'status': 'running', 'ancestor': 'selenium/hub'})
            for hub in hubs:
                try:
                    if hub.attrs['HostConfig']['PortBindings']['4444/tcp'][0]['HostPort'] == kwargs['ports'][4444]:
                        hub.start()
                        return hub
                except:
                    pass
            params['ports'] = {4444: kwargs['ports']}
        else:
            params['ports'] = {4444: None}

    elif re.search('(^selenium/node)', image):
        params['auto_remove'] = True
        doc.containers.run(image, **params)
        return 'node started'

    print('params:')
    print(params)
    doc.containers.run(image, **params)
    return 'container started'
    #job = self.queue.enqueue(f=doc_run, args=(image,), kwargs=params)
    #return job.get_id()


def stop_container(id):
    container = doc.containers.get(id)
    container.stop()
    return 'ok'


def start_container(name):
    container = doc.containers.get(name)
    container.start()
    return 'ok'


def pull_image(image):
    doc.images.pull(image)


def testq():
    print('start queue testing, meeting morpheus')
    time.sleep(10)
    print('I got great CONVERSATION WITH MORPHEUS')


class Q:
    def start_container(self, image=None, **kwargs):
        if image:
            self.queue.enqueue(f=start_new_container, args=(image,), kwargs=kwargs)
        if 'name' in kwargs:
            name = kwargs.pop('name')
            self.queue.enqueue(f=start_container, args=(name,))

#    def start_container(self, image, **kwargs):
#        self.queue.enqueue(f=start_new_container, args=(image,), kwargs=kwargs)


    def stop_container(self, id):
        self.queue.enqueue(f=stop_container, args=(id,))

    def pull_image(self, image):
        job = self.queue_slow.enqueue(
            f=pull_image, args=(image,), ttl=3000, result_ttl=50000, timeout=1200
        )
        return job.id

    def test_queue(self):
        self.queue.enqueue(f=testq, result_ttl=5000)

    def stop_worker(self):
        self.queue.enqueue(f=kill)

    def __init__(self):
        self.queue = Queue(connection=workers.conn)
        self.queue_slow = Queue(connection=workers.conn, name='slow')


def doc_run(image, args):
    doc.containers.run(image, **args)