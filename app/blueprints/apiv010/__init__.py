from flask import Blueprint
from flask_restful import Api


api_bp = Blueprint('apiv010', __name__)
api = Api(api_bp)

from . import tower
