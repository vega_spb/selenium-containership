from . import api
from app.queue.tower import doc
import docker as DOCKER
from flask import jsonify, request
from flask_restful import reqparse, abort, Resource

from app.queue.tower import Q
queue = Q()


class ListRunning(Resource):

    def get(self):
        response = dict()
        for cont in doc.containers.list():
            cont_details = {
                'id': cont.id,
                'status': cont.status,
                'image': str(cont.image)[9:-2]
            }
            response[cont.name] = cont_details
        if response == {}:
            response = {'error': 'no containers running'}
        print(response)
        return response


class ListAll(Resource):

    def get(self):
        response = dict()
        for cont in doc.containers.list(all=True):
            cont_details = {
                'id': cont.id,
                'status': cont.status,
                'image': str(cont.image)[9:-2]
            }
            response[cont.name] = cont_details
        if response == {}:
            response = {'error': 'no containers available'}
        return response


class ListSleeping(Resource):

    def get(self):
        response = dict()
        for cont in doc.containers.list(all=True, filters={'status': 'exited'}):
            cont_details = {
                'id': cont.id,
                'status': cont.status,
                'image': str(cont.image)[9:-2],
                'exitcode': cont.wait()['StatusCode']
            }
            response[cont.name] = cont_details

        for cont in doc.containers.list(all=True, filters={'status': 'paused'}):
            cont_details = {
                'id': cont.id,
                'status': cont.status,
                'image': str(cont.image)[9:-2]
            }
            response[cont.name] = cont_details

        if response == {}:
            response = {'error': 'no containers available'}
        return response


class Details(Resource):

    def get(self, id):
        cont = doc.containers.get(id)
        response = cont.attrs
        return jsonify(response)


class Stop(Resource):

    def stop(self, type, quantity):
        if quantity == 1:
            queue.stop_container(type)
        if quantity == 'All':
            for cont in doc.containers.list(filters={'ancestor': type, 'status': 'running'}):
                print(cont.name)
                queue.stop_container(cont.name)
        return 'OK'

    def get(self, id=None):
        if id == None:
            return {'I cant': 'stop'}
        else:
            self.stop(id, 1)

    def post(self):
        json_data = request.get_json(force=True)
        id = json_data['type']
        quantity = json_data['quantity']
        if quantity == 1:
            self.stop(id, 1)
        if quantity == 'All':
            self.stop(id, 'All')


class Start(Resource):

    def get(self):
        return {'Nothing here': 'But us, chickens'}

    def post(self):
        json_data = request.get_json(force=True)
        if 'name' in json_data:
            try:
                cont = doc.containers.get(json_data['name'])
                queue.start_container(name=cont)
                return {'ok': 'start queued'}
            except DOCKER.errors.NotFound:
                return {'error': 'container %s not found' % json_data['name']}
            except Exception as e:
                return {'error': 'Something went very wrong: %s' % e}
        if 'quantity' in json_data:
            if json_data['quantity'].isdigit():
                quantity = int(json_data.pop('quantity'))
                image = json_data.pop('image')
                for _ in range(quantity):
                    queue.start_container(image, **json_data)
                return {'ok': 'start queued'}
            else:
                return {'error': 'Are you mad? quantity isnt num: %s' % json_data['quantity']}


class Greeting(Resource):

    def get(self):
        response = {'greeting': 'Hola Amigo!'}
        return jsonify(response)


api.add_resource(Greeting, '/')
api.add_resource(ListRunning, '/list', '/list/running')  # переписать в один класс
api.add_resource(ListAll, '/list/all')                   # угу
api.add_resource(ListSleeping, '/list/sleeping')         # ага
api.add_resource(Details, '/details/<id>')
api.add_resource(Stop, '/stop', '/stop/<id>')
api.add_resource(Start, '/start')


class Test(Resource):

    def get(self, var=None):
        if var == None:
            abort(404)
        print(var)
        response = {'var': var}
        return response


api.add_resource(Test, '/test/', '/test/<var>')

