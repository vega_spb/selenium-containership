from flask import Flask
import os


app = Flask(__name__)

"""
redis queue dashboard
"""
import rq_dashboard
app.config.from_object(rq_dashboard.default_settings)
app.register_blueprint(rq_dashboard.blueprint, url_prefix="/rq")

"""
Config stuff
"""
app.config.from_pyfile('../config/default.py')
if os.path.isfile('./config/config.py'):
    app.config.from_pyfile('../config/config.py')

"""
flask-restful stuff
"""
#from flask_restful import Api
#from app.blueprints.apiv010 import apiv010_bp
# api = Api(app)
# api.init_app(app)
# api.add_resource(List, '/list/')
# api.add_resource(Greeting, '/')


'''
JINJA-2 custom filters
'''
from app.jinja_stuff import *
#import app.jinja_stuff
app.jinja_env.tests['list'] = is_list
app.jinja_env.tests['dict'] = is_dict

"""
Routes
"""
from app.routes import *


"""
Blueprints
"""
from app.blueprints.apiv010 import api_bp as apiv010_bp
app.register_blueprint(apiv010_bp, url_prefix='/api/v0.10')

"""
workers
"""
from app.queue.worker import workers
workers.dadada()
