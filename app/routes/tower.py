from flask import render_template, request, redirect, url_for
from app import app
from app.forms.tower import *
from app.queue.tower import Q
import re
import docker as DOCKER
from urllib.parse import urlparse

q = Q()

# redis queue
from rq import Queue
from app.queue.worker import workers
queue = Queue(connection=workers.conn)
queue_slow = Queue(connection=workers.conn, name='slow')

doc = DOCKER.from_env()

#from multiprocessing import current_process
#from app.dark_tower import Shultz

#print('printing from flask routes/tower, pid is ', current_process().pid)
#dantist = Shultz()

@app.route('/')
def index():
    url = ''
    #regex = '(?:http.*://)?(?P<host>[^:/ ]+).?(?P<port>[0-9]*).*'
    #url = re.search(regex, request.host_url)
    #host = url.group('host')
    if request.headers.getlist("HOST"):
        host = request.headers.getlist("HOST")[0]
    else:
        # regex = '(?:http.*://)?(?P<host>[^:/ ]+).?(?P<port>[0-9]*).*'
        # url = re.search(regex, request.host_url)
        # host = url.group('host')
        host = urlparse(request.host_url).hostname
    containers = doc.containers.list()
    print(request.host_url)
    print(containers)
    return render_template('containers_running.html', containers=containers, address=host)


@app.route('/details/<name>')
def container_details(name):
    try:
        cc = doc.containers.get(name)
    except doc.errors.NotFound:
        cc = None
    except Exception as e:
        print('Something went VERY wrong!!! Watch exception:')
        print(e)
    pass
    print(cc)
    return render_template('container_details.html', container=cc)


@app.route('/start', methods=['GET', 'POST'])
def start():
    action = 'start'
#    network = 'grid'
#    name = ''
#    cont_env = ['HUB_HOST=selenium-hub']
    ports = {5900: None}
    params = dict()
    if 'network' in request.args:
        params['network'] = request.args['network']
    if 'ports' in request.args:
        params['ports'] = request.args['ports']
    if 'name' in request.args:
        params['name'] = request.args['name']
    if 'image' in request.args:
        image = request.args['image']
        q.start_container(image, **params)
#        if re.search('(selenium/hub)', image) and ports == {5900: None}:
#            ports = {4444: 4444}

#        cont = doc.containers.run(image, network=network, environment=cont_env, ports=ports, name=name, detach=True)
#        job = queue.enqueue(
#            f=doc.containers.run, args=(image,), kwargs=({'network': network, 'environment': cont_env, 'ports': ports, 'name': name, 'detach': True})
#            )
#        print(job)
#        print(job.get_id())

    images = doc.images.list()
    return render_template('containers_available.html', containers=images, action=action)


@app.route('/stop', methods=["GET", 'POST'])
def stop():
    if 'container' in request.args:
        id = request.args['container']
        print(id)
        q.stop_container(id)
    return redirect('/', code=302)
    pass


@app.route('/pull', methods=['GET', 'POST'])
def pull():
    form = PullImage()
    if request.method == 'POST':
#        if form.validate_on_submit():
        image = form.image_name.data
        try:
            q.pull_image(image)
            return redirect(url_for('start'))
        except Exception as e:
            print('Whoops, something went wrong, exception %s' % e)
    return render_template('pull.html', form=form)
    pass


@app.route('/delete', methods=['GET', 'POST'])
def delete():
    action = 'delete'

    if 'image' in request.args:
        image = request.args['image']
        print(image)
        cont = doc.containers.run(image, detach=True)
        print(cont)
    images = doc.images.list()
    return render_template('containers_available.html', containers=images, action = action)


import time





@app.route('/qtest')
def qtest():
    q.test_queue()
    print(job.get_id)
    return redirect('/')