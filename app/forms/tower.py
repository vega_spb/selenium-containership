from flask import Flask
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired


class PullImage(FlaskForm):
    image_name = StringField('Image Name', validators=[DataRequired()])


class StartContainer(FlaskForm):
    network = StringField('NetworkName', default='grid')