# Welcome to Selenium Containership project
Это проект для управления докер-контейнерами и формирования селениум хаба для проведения тестов.

Сформировать хаб по умолчанию возможно с помощью web-интерфейса, а так же доступно rest API.
На данный момент реализовано API v 0.10.

### Работа с апи:
Апи доступно по адресу `/api/vX.XX/`, где X.XX -верися АПИ ( на данный момент 0.10) 

#### API v 0.10:
* `/api/v0.10/list` и `/api/v0.10/list/running` - вернется список запущеных контейнеров или сообщение о том что контейнеры не запущены.
пример:
```json
{
    "error": "no containers running"
}
```
```json
{
    "cocky_gates": {
        "id": "57782e9f4571f16a265b1f54192f63fc446758beac5e435f0bb00bf833bfe700",
        "status": "running",
        "image": "selenium/node-firefox-debug:latest"
    },
    "jovial_carson": {
        "id": "f9ce586572a69f95f02fb7019f8d0b6b0ed2063ba0d45cc3dcdc1910a1cd0a8f",
        "status": "running",
        "image": "selenium/node-chrome-debug:latest"
    },
    "selenium-hub": {
        "id": "3527394209c6e2def746489c665218cab58e797ff905028f82d256d1882720c7",
        "status": "running",
        "image": "selenium/hub:latest"
    }
}
```
Так же есть методы `/api/v0.10/list/all` и `/api/v0.10/list/sleeping`

* `/api/v0.10/stop/` есть два варианта использования:

GET: `/api/v0.10/stop/<container_id>` - например `/api/v0.10/stop/jovial_carson` остановим контейнер с таким именем. 
или так же по id: `/api/v0.10/stop/f9ce586572a69f95f02fb7019f8d0b6b0ed2063ba0d45cc3dcdc1910a1cd0a8f`

POST: этот метод дает нам немного больше свободы.

```json 
{
    "type": "имя_контейнера",
    "quantity": 1
}
```

или

```json
{
    "type": "образ_из/которого:собран_контейнер",
    "quantity": "All"
}
```
Например:

```json 
{
    "type": "cocky_gates",
    "quantity": 1
}
```
остановит контейнер с именем cocky_gates. А 

```json 
{
    "type": "selenuim/node-frefox-debug:latest",
    "quantity": "All"
}
```
остановит все контейнеры, запущенные из этого (selenium/node-firefox-debug) образа.

* Запуск контейнеров: `/api/v0.10/start`
Принимает метод POST. Как обычно пушим json, 2 варианта:

```json 
{
    "image": "имя_контейнера",
    "quantity": N
}
```
Запустит N контейнеров из указанного образа.
Например:

```json 
{
    "image": "selenium/node-chrome-debug:latest",
    "quantity": 3
}
```
запустит 3 контейнера из образа selenium/node-chrome-debug:latest с параметрами по умолчанию.
Также можно запустить созданный ранее контейнер по имени:

```json 
{
    "name": "имя_контейнера"
}
```
Например:

```json 
{
    "name": "selenuim-hub"
}
```
Попытается запустить контейнер с именем selenium-hub, но т.к. он уже запущен просто ничего не сделает.
По умолчанию контейнеры запускаются для хаба по умолчанию selenuim-hub и сети с названием grid.
Через web-интерфейс можно подключиться через vnc к debug-контейнерам.

### Деплой на сервер c Debian 9 ( stretch ).
```bash
apt-get install python3.5 virtualenv redis
addusr worker
mkdir -p /usr/necronomicon
chown -R worker:www-data /usr/necronomicon
su worker
cd /usr/necronomicon
git clone git@gitlab.com:kopacb/selenium-containership.git
cd /usr/necronomicon
virtualenv --no-site-packages --distribute -p /usr/bin/python3.5 venv
source venv/bin/activate
pip install -r requirements.txt
deactivate
```
Далее вам необходимо запустить проект вашим любимым демонизатором ( я вот люблю pm2 )
`pm2 start /usr/necronomicon/selenium-containership/run.py --interpreter /usr/necronomicon/selenium-containership/venv/bin/python --name selenium-containership`
Если вы хотите использовать VNC дабы иметь возможность посмотреть в контейнер то необходимо установить noVNC:
```bash
cd /var/www/
git clone https://github.com/novnc/noVNC.git 
chown -R www-data:www-data /var/www/noVNC
```
В файле HELP/nginx/default вы можете найти базовый конфиг для nginx. 



The project based on:
* Flask http://flask.pocoo.org/
* Docker SDK for Python https://github.com/docker/docker-py
* noVNC https://github.com/novnc/noVNC
